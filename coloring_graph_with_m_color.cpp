//coloring_graph_with_m_color
//Time : v*m
#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;

vector <ll> r[51];
ll color[51];
ll t ,n , e , m;
bool isSafe(ll x , ll cc)
{
    for(auto i : r[x])
    {
        if(cc == color[i])
            return false;
    }
    return true;
}
bool mcl(ll v)
{
    if(v == n)
        return true;
    Rep(i , m)
    {
        if(isSafe(v , i))
        {
            color[v]=i;
            if(mcl(v+1))
                return true;
            color[v]=-1;
        }
    }
    return false;
}
int main()
{
   // Test;
    cin>>t>>n>>m>>e;
    while(t--)
    {
        Set(color ,-1);
        Rep(i , e)
        {
            ll x ,y;
            cin>>x>>y;
            x-- , y--;
            r[x].push_back(y);
            r[y].push_back(x);
        }
        cout<<mcl(0)<<endl;
        Rep(i , n)
            r[i].clear();
    }
    return 0;
}